﻿(function () {
    'use strict';

    function homeController() {
        var ctrl = this;
        ctrl.tableData = [];
    }

    homeController.$inject = [];

    angular.module('app')
        .component('homePage', {
            templateUrl: '/App/Home/home.html',
            controller: homeController
        })
})()