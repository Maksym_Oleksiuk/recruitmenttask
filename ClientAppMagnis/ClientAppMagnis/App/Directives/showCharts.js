﻿
//Directive for change html page.
(function () {
    function showCharts($timeout, appConfig, homeService, websocketService) {
        return {
            restrict: 'A',
            scope: {
                showCharts: "="
            },
            link: function (scope, element, attributes) {

                var aggregateDiagramPoints = {};
                console.log(scope);

                //used $timeout for waiting upload page
                $timeout(function () {
                    homeService.getData().then(function (response) {
                        homeService.data = response;

                        //start point for creating charts
                        prepareData(response);
                    });

                    //set new request with interval 'appConfig.delay'
                    setInterval(function () {
                        var data = null;
                        websocketService.websocketImitation().then(function (response) {
                            if (response !== null) {
                                homeService.data = response;
                                prepareData(response);
                                toastr["success"]("New data was come.");
                            }
                        });
                    }, appConfig.delay);

                });

                //creating aggregate chart
                function aggregateDiagram() {
                    var dataForDiagram = [];
                    for (var key in aggregateDiagramPoints) {
                        var newDataForChart = {
                            "type": "scatter",
                            "legendText": key,
                            "showInLegend": "true",
                            "dataPoints": aggregateDiagramPoints[key]
                        }
                        dataForDiagram.push(newDataForChart);
                    }
                    var chart = new CanvasJS.Chart('Aggregate', {
                        theme: "light1",
                        animationEnabled: false,
                        title: {
                            text: "Aggregate"
                        },
                        legend: {
                            horizontalAlign: "right",
                            verticalAlign: "top"
                        },
                        data: dataForDiagram
                    });
                    chart.render();
                }

                //craeting  M15, H1, H4 and D1 charts
                function generatingCharts(key, data) {
                    var dataPoints = [];
                    aggregateDiagramPoints[key] = [];
                    for (var i = 0; i < data.length; i++) {
                        dataPoints.push({ label: data[i].Key, y: data[i].Value });

                        //Save data for agregate diagram
                        aggregateDiagramPoints[key].push({ label: data[i].Key, y: data[i].Value });
                    }

                    var chart = new CanvasJS.Chart(key, {
                        theme: "light1",
                        animationEnabled: false,	
                        title: {
                            text: key
                        },
                        data: [
                            {
                                type: "column",
                                color: "blue",
                                dataPoints: dataPoints
                            },
                        ]
                    });
                    chart.render();
                }

                //start point for creating charts
                function prepareData(response) {
                    for (var key in response) {

                        //only for M15, H1, H4 and D1 because their legths = 8
                        if (response[key].length > 4) {
                            generatingCharts(key, response[key]);
                        }
                    }

                    //generate aggregate Diagram;
                    aggregateDiagram();

                    //change data in table
                    scope.$parent.$ctrl.tableData = response.SymbolIndices;
                }
            }
        }
    }

    showCharts.$inject = ['$timeout', 'appConfig', 'homeService', 'websocketService'];

    angular.module('showChartsDirective', [])
        .directive('showCharts', showCharts);

})();