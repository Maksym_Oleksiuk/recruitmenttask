﻿(function () {
    'use strict';
    function websocketService(homeService) {
        var vm = this;
        vm.newData = {};

        //get and compire data
        vm.websocketImitation = function () {
            return homeService.getData().then(function (response) {
                if (response.LastCalcTime !== homeService.data.LastCalcTime) {
                    return response;
                }
                else {
                    return null;
                }
            });
        }
    }

    websocketService.$inject = ['homeService'];

    angular
        .module('websocket.service', [])
        .service('websocketService', websocketService);
})();