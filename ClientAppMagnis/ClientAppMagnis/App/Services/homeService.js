﻿(function () {
    'use strict';
    function homeService($http, appConfig) {
        var vm = this;
        vm.data = {};

        //set toastr options
        toastr.options = appConfig.toastrOptions;

        vm.getData = function () {
            return $http.get(appConfig.externalServer).then(onSuccess, onError);
            function onSuccess(response) {
                return response.data;
            }
            function onError(response) {
                toastr["error"]("Error while retrieving new data.")
            }
        }
    }

    homeService.$inject = ['$http', 'appConfig'];

    angular
        .module('home.service', [])
        .service('homeService', homeService);
})();