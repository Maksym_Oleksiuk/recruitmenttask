﻿/*app modules*/
angular.module('app', [
    'showChartsDirective',
    'home.service',
    'websocket.service'
])
    .constant('appConfig', {
        'toastrOptions': {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        },
        'externalServer': 'http://91.194.90.62:20019/fxservice/strongweak', //external server address
        'delay' : 5000 //delay(in ms) before sending new request on the external server


    });